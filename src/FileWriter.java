import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileWriter {
	
	public FileWriter(){
		fos = null;
	}
	
	public void createFile(String filename){
		
		this.closeFile();
		
		File outputFile = new File(filename);
		try{
			this.fos = new FileOutputStream(outputFile);
			
			if(!outputFile.exists()){
				outputFile.createNewFile();		
			}
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}
	
	public void writeToFile(byte[] contentInBytes){
		try{
			fos.write(contentInBytes);
			fos.flush();
		}
		catch(IOException e){
			e.printStackTrace();
		}

		System.out.println("Done");
	}
	
	public void closeFile(){
		if(fos == null){
			return ;
		}
		try{
			fos.close();
			fos = null;
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}
	
	private FileOutputStream fos;
}

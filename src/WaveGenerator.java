import java.nio.ByteBuffer;

public class WaveGenerator {

	public static void main(String[] args) {
		System.out.println("Acesta este un program ce genereaza semnale audio (float data).");
		
		//generate signal
		
		float amp = 1;
		int freq = 900;
		int sampRate = 44100;
		int seconds = 15;
		
		WaveSignal signal = new WaveSignal(amp,freq);
		float[] signalSamples = signal.getSineWave(sampRate, seconds);
		EndiannessSwapper.swap(signalSamples); //change to little endian
		
		//convert to byte[]
		byte[] signalData = new byte[4*signalSamples.length];
		for(int i=0;i<signalSamples.length;i++){
		    byte[] sample = convertToByteArray(signalSamples[i]);
		    for(int j=0;j<4;j++){
		    	signalData[4*i+j] = sample[j];
		    	
		    }
		}
		
		
		//write to file
		FileWriter fw = new FileWriter();
		fw.createFile("C:/Users/Vlad/Desktop/signal.wav");
		fw.writeToFile(getWavHeader(72+signalData.length,1,sampRate,signalData.length,(byte)32));
		fw.writeToFile(signalData);
		fw.closeFile();

	}
	
    private static byte[] convertToByteArray(float value) {
        return ByteBuffer.allocate(4).putFloat(value).array();
    }

	//https://stackoverflow.com/questions/9179536/writing-pcm-recorded-data-into-a-wav-file-java-android
    private static byte[] getWavHeader(int totalDataLen,int channels,int sampleRate,int totalAudioLen, byte bitsPerSample){
    	
    	int byteRate = sampleRate*bitsPerSample*channels/8;
    	
    	byte[] header = new byte[44];
        header[0] = 'R';  // RIFF/WAVE header
        header[1] = 'I';
        header[2] = 'F';
        header[3] = 'F';
        header[4] = (byte) (totalDataLen & 0xff);
        header[5] = (byte) ((totalDataLen >> 8) & 0xff);
        header[6] = (byte) ((totalDataLen >> 16) & 0xff);
        header[7] = (byte) ((totalDataLen >> 24) & 0xff);
        header[8] = 'W';
        header[9] = 'A';
        header[10] = 'V';
        header[11] = 'E';
        header[12] = 'f';  // 'fmt ' chunk
        header[13] = 'm';
        header[14] = 't';
        header[15] = ' ';
        header[16] = 16;  // 4 bytes: size of 'fmt ' chunk
        header[17] = 0;
        header[18] = 0;
        header[19] = 0;
        header[20] = 3;  // format = 1
        header[21] = 0;
        header[22] = (byte) channels;
        header[23] = 0;
        header[24] = (byte) (sampleRate & 0xff);
        header[25] = (byte) ((sampleRate >> 8) & 0xff);
        header[26] = (byte) ((sampleRate >> 16) & 0xff);
        header[27] = (byte) ((sampleRate >> 24) & 0xff);
        header[28] = (byte) (byteRate & 0xff);
        header[29] = (byte) ((byteRate >> 8) & 0xff);
        header[30] = (byte) ((byteRate >> 16) & 0xff);
        header[31] = (byte) ((byteRate >> 24) & 0xff);
        header[32] = (byte) (2 * 16 / 8);  // block align
        header[33] = 0;
        header[34] = bitsPerSample;  // bits per sample
        header[35] = 0;
        header[36] = 'd';
        header[37] = 'a';
        header[38] = 't';
        header[39] = 'a';
        header[40] = (byte) (totalAudioLen & 0xff);
        header[41] = (byte) ((totalAudioLen >> 8) & 0xff);
        header[42] = (byte) ((totalAudioLen >> 16) & 0xff);
        header[43] = (byte) ((totalAudioLen >> 24) & 0xff);
        
        return header;
    }

}

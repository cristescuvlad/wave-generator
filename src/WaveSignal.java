
public class WaveSignal {
	
	public WaveSignal(float amp, int freq){
		this.amp = amp;
		this.freq = freq;
	}
	
	public float[] getSineWave(int sampRate, int seconds){
		
		float[] signal = new float[seconds*sampRate];
		
		for(int i=0;i<seconds*sampRate;i++){
			signal[i] = amp*(float)Math.sin(2*Math.PI*(float)freq*(float)i/sampRate);
		}
		
		return signal;
	}
	
	private float amp = 0;
	private int freq = 0;
}
